import {GraphQLObjectType, GraphQLSchema} from 'graphql';

import CommentQL from './comment';
import FilterQL from './filter';
import PictureQL from './picture';
import UserQL from './user';
import SearchQL from './search';

export default new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'RootQuery',
        fields: () => {
            return {
                /*eslint-disable*/
                ...CommentQL.CommentQueries,
                ...FilterQL.FilterQueries,
                ...PictureQL.PictureQueries,
                ...UserQL.UserQueries,
                ...SearchQL.SearchQueries
                /*eslint-enable*/
            };
        }
    }),
    mutation: new GraphQLObjectType({
        name: 'RootMutation',
        fields: () => {
            return {
                 /*eslint-disable*/
                ...CommentQL.CommentMutations,
                ...FilterQL.FilterMutations,
                ...PictureQL.PictureMutations,
                ...UserQL.UserMutations
                /*eslint-enable*/
            };
        }
    })
});
