import FilterQueries from './filter-queries';
import FilterMutations from './filter-mutations';

export default {FilterQueries, FilterMutations};
