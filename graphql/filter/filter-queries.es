import {
    GraphQLList
} from 'graphql';

import {FilterModel} from '../../models/filter';
import FilterType from './filter-type';

export default {
    filters: {
        type: new GraphQLList(FilterType),
        args: {},
        resolve: FilterModel.getFilters
    }
}
