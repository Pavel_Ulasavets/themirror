import {
    GraphQLString,
    GraphQLObjectType
} from 'graphql';

export default new GraphQLObjectType({
    name: 'FilterType',
    fields: () => {
        return {
            name: {
                type: GraphQLString
            }
        };
    }
});
