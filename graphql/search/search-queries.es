import { GraphQLString, GraphQLList } from 'graphql';
// models
import { UserModel } from  '../../models/user';

export default {
    similarImages: {
        type: new GraphQLList(GraphQLString),
        args: {
            targetImage: {
                type: GraphQLString
            }
        },
        resolve: (_, args) => (UserModel.findSimilarImages(args.targetImage))
    }
};
