import CommentQueries from './comment-queries';
import CommentMutations from './comment-mutations';

export default {CommentQueries, CommentMutations};
