import {
    GraphQLID,
    GraphQLObjectType,
    GraphQLInputObjectType,
    GraphQLInt,
    GraphQLString,
    GraphQLList
} from 'graphql';

const fields = {
    id: {
        type: GraphQLID
    },
    text: {
        type: GraphQLString
    },
    authorId: {
        type: GraphQLID
    }
};

export const OutputCommentType = new GraphQLObjectType({
    name: 'OutputCommentType',
    fields: () => ({
        /*eslint-disable*/...fields,/*eslint-enable*/
        comments: {
            type: new GraphQLList(OutputCommentType)
        }
    })
});

export const InputCommentType = new GraphQLInputObjectType({
    name: 'InputCommentType',
    fields: () => ({
        /*eslint-disable*/...fields,/*eslint-enable*/
        comments: {
            type: new GraphQLList(InputCommentType)
        }
    })
})
