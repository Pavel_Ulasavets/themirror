import PictureQueries from './picture-queries';
import PictureMutations from './picture-mutations';

export default {PictureQueries, PictureMutations};
