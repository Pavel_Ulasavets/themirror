// libs
import {
    GraphQLID,
    GraphQLObjectType,
    GraphQLInputObjectType,
    GraphQLInt,
    GraphQLString,
    GraphQLList
} from 'graphql';
import {InputCommentType, OutputCommentType} from '../comment/comment-type';

const fields = {
    name: {
        type: GraphQLString
    },
    appliedFilters: {
        type: new GraphQLList(GraphQLID)
    },
    likedBy: {
        type: new GraphQLList(GraphQLID)
    },
    imagePath: {
        type: GraphQLString
    }
};

export const OutputPictureType = new GraphQLObjectType({
    name: 'OutputPictureType',
    fields: () => ({
        /*eslint-disable*/
        ...fields,
        /*eslint-enable*/
        comments: {
            type: new GraphQLList(OutputCommentType)
        },
    })
});

export const InputPictureType = new GraphQLInputObjectType({
    name: "InputPictureType",
    fields: () => ({
        /*eslint-disable*/
        ...fields,
        /*eslint-enable*/
        comments: {
            type: new GraphQLList(InputCommentType)
        },
    })
})
