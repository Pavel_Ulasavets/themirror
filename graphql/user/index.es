import UserQueries from './user-queries';
import UserMutations from './user-mutations';

export default {UserQueries, UserMutations};
