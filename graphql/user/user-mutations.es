import {
    GraphQLNonNull,
    GraphQLID
} from 'graphql';

import {UserModel} from '../../models/user';
import {UserInputType, UserOutputType} from './user-type';

export default {
    updateUser: {
        type: UserOutputType,
        args: {
            _id: {
                type: new GraphQLNonNull(GraphQLID)
            },
            user: {
                type: UserInputType
            }
        },
        resolve: (root, {_id, user}) => { return UserModel.updateUser(_id, user); }
    },
    removeUser: {
        type: UserOutputType,
        args: {
            _id: {
                type: new GraphQLNonNull(GraphQLID)
            }
        },
        resolve: (root, {_id}) => { return UserModel.removeUser(_id); }
    }
};
