// libs
import {
    GraphQLID,
    GraphQLObjectType,
    GraphQLInputObjectType,
    GraphQLInt,
    GraphQLString,
    GraphQLBuffer,
    GraphQLList
} from 'graphql';

import {InputPictureType, OutputPictureType} from '../picture/picture-type';

const fields = {
    email: {
        type: GraphQLString
    },
    fullname: {
        type: GraphQLString
    },
    username: {
        type: GraphQLString
    },
    password: {
        type: GraphQLString
    },
    subscriptions: {
        type: new GraphQLList(GraphQLID)
    }
};

export const UserOutputType = new GraphQLObjectType({
    name: 'UserOutputType',
    fields: () => ({
        /*eslint-disable*/...fields,/*eslint-enable*/
        pictures: {
            type: new GraphQLList(OutputPictureType)
        },
        logo: {
            type: OutputPictureType
        }
    })
});

export const UserInputType = new GraphQLInputObjectType({
    name: 'UserInputType',
    fields: () => ({
        /*eslint-disable*/...fields,/*eslint-enable*/
        salt: {
            type: GraphQLString
        },
        pictures: {
            type: new GraphQLList(InputPictureType)
        },
        logo: {
            type: InputPictureType
        }
    })
})
