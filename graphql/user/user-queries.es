import {
    GraphQLList,
    GraphQLID,
    GraphQLNonNull
} from 'graphql';

import {UserModel} from '../../models/user';
import {UserOutputType} from './user-type';

export default {
    users: {
        type: new GraphQLList(UserOutputType),
        resolve: UserModel.getAllUsers
    },
    user: {
        type: UserOutputType,
        args: {
            id: {
                type: new GraphQLNonNull(GraphQLID)
            }
        },
        resolve: (_, args) => (UserModel.findUserById(args.id))
    }
};
