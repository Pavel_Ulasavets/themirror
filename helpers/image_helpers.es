/**
 * calculates a perceptual hash value for the passed image
 * @param  {ndarray} imgNDArray
 * @return {Promise}
 */
export function pHash(imgNDArray) {
    return Promise.resolve();
}

/**
 * calculates the hamming distance between two hashes
 * @param  {[type]} hashA
 * @param  {[type]} hashB
 * @return {Number}
 */
export function hammingDistance(hashA, hashB) {
    return 0;
}
