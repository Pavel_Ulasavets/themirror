module.exports = {
    context: __dirname + '/ui/src',
    entry: './index.es',
    output: {
        path: __dirname + '/content/public',
        filename: 'app.bundle.js',
        publicPath: ''
    },
    module: {
        loaders: [
            { test: /\.es$/, loaders: ['babel-loader'] },
            { test: /\.scss$/, loaders: ['style-loader', 'css-loader', 'sass-loader'] }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.es', '.css', '.scss']
    }
};
