// external modules
import path from 'path';
import { Router } from 'express';
import { graphql } from 'graphql';
import auth from '../auth';
import schema from '../graphql';
import { StatusCodes, StatusMessages } from '../constants';

const router = Router();

router.use(auth.initialize());
router.use(auth.session());

router.get('/login/instagram', auth.authenticate('instagram'));
router.get(
    '/login/instagram/callback',
    auth.authenticate('instagram', {
        successRedirect : '/',
        failureRedirect : '/login'
    })
);

router.get('/login/google', auth.authenticate('google', { scope: ['profile'] }));
router.get(
    '/login/google/callback',
    auth.authenticate('google', {
        successRedirect : '/',
        failureRedirect : '/login'
    })
);

router.get('*', (req, res) => {
    const isLoginPage = req.url === '/login';
    const isRegisterPage = req.url === '/register';

    if (!isLoginPage && !isRegisterPage && !req.isAuthenticated()) {
        res.redirect('/login');
    } else {
        res.setHeader('content-type', 'text/html');
        res.sendFile(path.join(__dirname, '../content', 'index.html'));
    }
});

router.post('/graphql', (req, res) => {
    if (req.isAuthenticated()) {
        graphql(schema, req.body)
            .then(
                (result) => {
                    res.send(result);
                }
            );
    } else {
        res
            .status(StatusCodes.UNAUTHORIZED)
            .send({ message: StatusMessages.ACCESS_DENIED });
    }
});

router.post('/register', (req, res, next) => {
    req.checkBody({
        email: {
            notEmpty: true,
            isEmail: {
                errorMessage: 'Invalid Email. Please try again'
            }
        },
        fullname: {
            notEmpty: true
        },
        password: {
            isLength: {
                options: [{ min: 8, max: 16 }],
                errorMessage: 'Must be between 8 and 16 characters long'
            }
        }
    });

    const validationErrors = req.validationErrors(true);

    if (validationErrors) {
        return res.status(StatusCodes.UNAUTHORIZED).send({ validationErrors, errMessage: null });
    }

    auth.authenticate('local-signup', function (err, user, info) {
        if (err) { return next(err); }

        if (!user) {
            res.status(StatusCodes.UNAUTHORIZED).send({ errMessage: info });
        } else {
            req.login(user, function(err) {
                if (err) { return next(err); }
                return res.status(StatusCodes.OK).send();
            });
        }
    })(req, res, next);
});

router.post('/login', (req, res, next) => {
    req.checkBody({
        email: {
            notEmpty: true,
            isEmail: {
                errorMessage: 'Invalid Email. Please try again'
            }
        },
        password: {
            isLength: {
                options: [{ min: 8, max: 16 }],
                errorMessage: 'Must be between 8 and 16 characters long'
            }
        }
    });

    const validationErrors = req.validationErrors(true);

    if (validationErrors) {
        return res.status('UNAUTHORIZED').send({ validationErrors });
    }

    auth.authenticate('local-login', function onAuthenticated(err, user, info) {
        if (err) { return next(err); }

        if (!user) {
            res.status(StatusCodes.UNAUTHORIZED).send({ errMessage: info });
        } else {
            req.login(user, function(err) {
                if (err) { return next(err); }
                return res.status(StatusCodes.OK).send();
            });
        }
    })(req, res, next);
});

router.post('/logout', (req, res, next) => {
    req.logout();
    req.session.destroy();
    res.status(StatusCodes.OK).send();
});

export default router;
