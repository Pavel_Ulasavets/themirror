// external modules
import express from 'express';
import expressSession from 'express-session';
import expressValidator from 'express-validator';
import mongoose from 'mongoose';
import connect from 'connect-mongo';
import bodyParser from 'body-parser';
import morgan from 'morgan';
// internal modules
import routes from './routes';
// constants
import { ContentTypes, StatusCodes, StatusMessages } from './constants';
import { app as appConfig, mongo as mongoConfig } from './configuration';

mongoose.connect(mongoConfig.url);

const app = express();

app.set('port', appConfig.port);
app.set('host', appConfig.host);
app.use(express.static(`${__dirname}/content/public`));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', req.headers.origin)   ;
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.use(morgan('dev'));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.text({ type: ContentTypes.GRAPHQL }));
app.use(bodyParser.json());
app.use(expressValidator());

const MongoStore = connect(expressSession);
const session = expressSession({
    name: appConfig.session.name,
    secret: appConfig.session.secret,
    store: new MongoStore({
        mongooseConnection: mongoose.connection
    }),
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 1000 * 60 * 60 * 24
    }
});

app.use(session);
app.use('/', routes);
// error handlers
app.use(function (req, res, next) {
    const code = StatusCodes.NOT_FOUND;
    return res.status(code).send({ message: StatusMessages[code], code });
});
app.use(function (err, req, res, next) {
    const code = StatusCodes.SERVER_ERROR;
    return res.status(code).send({ message: StatusCodes[code], code });
});



export default app;
