import http from 'http';
import app from './app';

const server = http.createServer(app);

server.listen(
    app.get('port'),
    app.get('host'),
    function (err) {
        /*eslint-disable*/
        if (err) {
            return console.error('Application Server:', err);
        }

        console.log('Application Server:', `started on ${app.get('host')}:${app.get('port')}`);
        /*eslint-enable*/
    }
);
