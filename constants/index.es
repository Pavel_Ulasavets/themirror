export const StatusCodes = {
    OK: 200,
    UNAUTHORIZED: 401,
    NOT_FOUND: 404,
    SERVER_ERROR: 500
};

export const StatusMessages = {
    [StatusCodes.UNAUTHORIZED]: 'Access denied. Please, log in',
    [StatusCodes.SERVER_ERROR]: "We're sorry. Server encoundered an internall error and was unable to complete your request",
    [StatusCodes.NOT_FOUND]: 'A route that you request isn\'t found. Please, check if your request route is correct'
};

export const ContentTypes = {
    JSON: 'application/json',
    GRAPHQL: 'application/graphql'
};

export const SearchConstants = {
    deviationLimit: 10
}
