import {Strategy} from 'passport-google-oauth20';

export function googleStrategyCreator(findUser, registerUser, options) {
    return new Strategy(
        options,
        function (accessToken, refreshToken, profile, done) {
            findUser(profile.id)
                .then(
                    function onUserFound(user) {
                        if (user) { return user; }

                        return registerUser({
                            id: profile.id,
                            fullname: profile.displayName,
                            token: accessToken
                        });
                    }
                )
                .then(
                    function onSuccess(user) {
                        done(null, user);
                    },
                    function onError(err) {
                        done(null, false, { message: err.message });
                    }
                )
        }
    );
}
