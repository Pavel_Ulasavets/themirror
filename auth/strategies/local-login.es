import { Strategy } from 'passport-local';

export function localLoginStrategyCreator(verifyCredentials) {
    return new Strategy(
        {
            usernameField: 'email',
            passwordField: 'password'
        },
        (email, password, done) => {
            verifyCredentials({email, password})
                .then(
                    function onSuccess(user) {
                        done(null, user);
                    },
                    function onFailed(err) {
                        done(null, false, { message: err.message });
                    }
                );
        }
    );
}
