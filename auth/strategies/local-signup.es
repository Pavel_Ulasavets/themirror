import { Strategy } from 'passport-local';

export function localSignUpStrategyCreator(registerUser) {
    return new Strategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        (req, email, password, done) => {
            registerUser({fullname: req.body.fullname, email, password})
                .then(
                    function onUserRegistrationSuccess(user) {
                        done(null, user);
                    },
                    function onUserRegistrationFailed(err) {
                        done(null, false, { message: err.message });
                    }
                )
        }
    );
}
