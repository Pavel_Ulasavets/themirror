import passport from 'passport';
// configuration
import { app as appConfig } from '../configuration';
// models
import {UserModel} from '../models/user';
// strategies
import { localLoginStrategyCreator } from './strategies/local-login';
import { localSignUpStrategyCreator } from './strategies/local-signup';
import { googleStrategyCreator } from './strategies/google';
import { instagramStrategyCreator } from './strategies/instagram';

const social = appConfig.social;

passport.serializeUser((user, done) => { UserModel.serializeUser(user).then((id) => done(null, id)); });
passport.deserializeUser((id, done) => { UserModel.deserializeUser(id).then((user) => done(null, user)); });

passport.use('local-login', localLoginStrategyCreator(UserModel.verifyCredentials));
passport.use('local-signup', localSignUpStrategyCreator(UserModel.registerUserDirectly));
passport.use('google', googleStrategyCreator(UserModel.findUserById, UserModel.registerUserIndirectly, social.google));
passport.use('instagram', instagramStrategyCreator(UserModel.findUserById, UserModel.registerUserIndirectly, social.instagram));

export default passport;
