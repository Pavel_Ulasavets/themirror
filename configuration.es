const appHost = 'localhost';
const appPort = 3030;

export const mongo = {
    url: `mongodb://localhost:2020/themirror`
};

export const app = {
    host: appHost,
    port: appPort,
    publicPath: `${__dirname}/public`,
    session: {
        secret: 'Holly Diver',
        name: 'themirror.session'
    },
    social: {
        google: {
            loginURL: `${appHost}/login/google`,
            clientID: '182257275600-50j2p0u0a9p181p90bat7c4tmk6s8rh5.apps.googleusercontent.com',
            clientSecret: 'rhlrwib0NtLpWs8fJbNzlT4q',
            callbackURL: `http://${appHost}:${appPort}/login/google/callback`
        },
        instagram: {
            clientID: '3362451bbba944c28d4cbb8ca176bdba',
            clientSecret: '68ecf0beb16f4e7683d7630ef103b0ea',
            loginURL: `http://${appHost}:${appPort}/login/instagram`,
            callbackURL: `http://${appHost}:${appPort}/login/instagram/callback`
        }
    }
};
