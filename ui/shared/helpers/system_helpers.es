
/**
 * converts an arbitrary array buffer to the appropriate string
 * @param  {ArrayBuffer} arrayBuffer
 * @return {String}
 */
export function arrayBufferToString(arrayBuffer) {
    return String.fromCharCode.apply(null, new Uint16Array(arrayBuffer));
}

/**
 * normalizes a file size
 * @param  {Number} sizeInBytes
 * @return {String}
 */
export function normalizeFileSize(sizeInBytes) {
    const postfixes = ['b', 'Kb', 'Mb', 'Gb'];
    let normalizedSize = sizeInBytes;
    let currentPostfix;

    for (currentPostfix of postfixes) {
        if (normalizedSize > 1024) {
            normalizedSize /= 1024;
        } else {
            break;
        }
    }

    return `${normalizedSize.toFixed(2)}${currentPostfix}`;
}
