import _ from 'lodash';
/**
 * [requestGraphQL description]
 * @param  {[type]} requestText [description]
 * @param  {[type]} options     [description]
 * @return {[type]}             [description]
 */
function request(requestText, options) {
    return new Promise(
        function onRequestCompleted(resolve, reject) {
            const xhr = new XMLHttpRequest();
            xhr.withCredentials = true;
            xhr.addEventListener('load', (e) => {
                let parsedData = null;
                const response = e.currentTarget.response;

                try {
                    parsedData = response ? JSON.parse(response) : {};
                } catch (err) {
                    reject(err);
                    return;
                }

                if (e.target.status === 200) {
                    resolve(parsedData);
                } else {
                    reject(parsedData);
                }
            });
            xhr.addEventListener('error', reject);
            xhr.open('POST', options.url, true);
            xhr.setRequestHeader('Content-Type', options.contentType);
            xhr.send(requestText);
        }
    );
}

export function requestToGraphQL(requestText, options = {}) {
    return request(requestText, _.assign({ contentType: 'application/graphql' }, options));
}

export function requestToAPI(requestText, options = {}) {
    return request(requestText, _.assign({ contentType: 'application/json' }, options));
}
