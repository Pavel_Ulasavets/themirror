/**
 * translate a specified key on a specified language
 * @param  {String} key - a key to be translated
 * @param  {String} lang - a target language
 * @return {String}
 */
export function translate(key, lang) {
    switch (key) {
    case 'DIC_EMAIL':
        return 'Email';
    case 'DIC_NAME':
        return 'Name';
    case 'DIC_FULLNAME':
        return 'Full Name';
    case 'DIC_USER_NAME':
        return 'User Name';
    case 'DIC_PASSWORD':
        return 'Password';
    case 'DIC_SIGNIN':
        return 'Sign in';
    case 'DIC_SIGNUP':
        return 'Sign up';
    default:
        return '';
    }
}
