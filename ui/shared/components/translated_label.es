import { Component } from 'react';
import { translate } from 'shared/helpers/language_helpers';

export default class TranslatedLabel extends Component {
    render() {
        return (<span>{translate(this.props.children, this.props.lang)}</span>);
    }
}
