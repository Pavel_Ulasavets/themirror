import './social-button-panel.scss';
import { Component } from 'react';
import SocialButton from './components/social-button';

export default class SocialButtonPanel extends Component {
    render() {
        const { buttons } = this.props;
        return (
            <div className='social-button-panel'>
            {
                buttons.map(
                    (btn) => (
                        <SocialButton key={btn.type} type={btn.type} text={btn.text} url={btn.url} />
                    )
                )
            }
            </div>
        );
    }
};
