import './social-button.scss';
import { Component } from 'react';

export default class SocialButton extends Component {
    render() {
        const { url, text, type } = this.props;
        return (
            <a href={url}>
                <div className={`social-button ${type}`}>
                    <div className="icon"></div>
                    <div className="text">
                        <span>{text}</span>
                    </div>
                </div>
            </a>
        );
    }
};
