export const DataServerSettings = {
    url: 'http://localhost:3030/'
};

export const SocialsSettings = {
    google: { url: '/login/google', text: 'Google+' },
    instagram: { url: '/login/instagram', text: 'Instagram' }
};
