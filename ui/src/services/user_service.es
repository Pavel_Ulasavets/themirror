// libs
import { requestToAPI } from 'shared/helpers/request_helpers';
// configurations
import { DataServerSettings } from '../../configuration';

/**
 * register a specified user on the server
 * @param  {Object} user
 * @return {Promise}
 */
export function registerUser(user) {
    return requestToAPI(
        JSON.stringify(user),
        { url: `${DataServerSettings.url}register` }
    );
}

/**
 * authenticate a user on the server
 * @param  {Object} credentials
 * @return {Promise}
} */
export function authenticateUser(credentials) {
    return requestToAPI(
        JSON.stringify(credentials),
        { url: `${DataServerSettings.url}login` }
    );
}
