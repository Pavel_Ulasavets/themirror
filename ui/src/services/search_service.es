// helpers
import { requestToGraphQL } from 'shared/helpers/request_helpers';
// configurations
import { DataServerSettings } from '../../configuration';

/**
 * seeks similar images by a specified one
 * @param  {String} - a base64 representation of the sought-for image
 * @return {Promise}
 */
export function searchSimilarImages(imgBase64) {
    return new Promise((resolve, reject) => {
        const query = `
            query RootQuery {
                similarImages(targetImage: "${imgBase64}")
            }
        `;

        requestToGraphQL(
            query,
            {
                url: `${DataServerSettings.url}graphql`,
                contentType: 'application/graphql'
            }
        )
        .then(
            (json) => resolve(json.data.similarImages),
            reject
        );
    });
}
