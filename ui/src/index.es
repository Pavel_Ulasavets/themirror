// external modules
import React from 'react';
window.React = React;
import ReactDOM from 'react-dom';
import { Router } from 'react-router';
// components
import { Provider } from 'react-redux';
// routes
import { getRoutes } from './routes/routes';
import { getHistory } from './routes/history';
// store
import store from './store/app_store';

class AppContainer extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Router history={ getHistory() } >
                    { getRoutes() }
                </Router>
            </Provider>
        );
    }
}

ReactDOM.render(<AppContainer/>, document.getElementById('react-app-container'));
