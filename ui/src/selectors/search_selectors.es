/**
 * selects a sought-for image URL
 * @state {Immutable.Map} - application state
 * @return {String}
 */
export function targetImageURL(state) {
    return state.getIn(['targetImage', 'url']);
}

/**
 * selects a sought-for image size
 * @param  {Immutable.Map} state
 * @return {String}
 */
export function targetImageSize(state) {
    return state.getIn(['targetImage', 'size']);
}

/**
 * selects an uploading error of sought-for image size
 * @param  {Immutable.Map} state
 * @return {String}
 */
export function targetImageUploadingError(state) {
    return state.getIn(['targetImage', 'error']);
}

/**
 * selects a result of search by image
 * @param  {[type]} state [description]
 * @return {[type]}       [description]
 */
export function searchResultList(state) {
    return state.get('searchResultList');
};




