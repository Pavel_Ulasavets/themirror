export const maxSizeOfUploadedImageInBytes = 600 * 1024; // in bytes

export const supportedImageExtensions = ['png', 'jpg', 'gif'];
