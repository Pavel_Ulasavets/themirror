import nskeymirror from 'nskeymirror';

export default nskeymirror({
    SET_TARGET_IMAGE: null,
    SET_SEARCH_RESULT_LIST: null
}, 'SearchActionTypes');
