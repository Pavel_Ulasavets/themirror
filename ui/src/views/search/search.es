import './search.scss';
import { connect } from 'react-redux';
// components
import { Component } from 'react';
import TargetImage from './components/target_image/target_image';
import SearchResultList from './components/search_result_list/search_result_list';
import SearchBar from './components/search_bar/search_bar';
// actions
import { searchSimilarImagesByFile, searchSimilarImagesByURL } from '../../actions/search_actions';
// selectors
import * as SearchSelectors from '../../selectors/search_selectors';

class Search extends Component {
    render() {
        const { targetImageURL, targetImageSize, targetImageUploadingError, searchResultList, onImageUploaded, onSearchByURLActivated } = this.props;

        return (
            <div className="search-container">
                <SearchBar onImageUploaded={onImageUploaded} onSearchByURLActivated={onSearchByURLActivated} />
                <TargetImage src={targetImageURL} size={targetImageSize} error={targetImageUploadingError} />
                <SearchResultList list={searchResultList} />
            </div>
        );
    }
}

export default connect(function (state) {
    const searchState = state.get('search');
    return {
        targetImageURL: SearchSelectors.targetImageURL(searchState),
        targetImageSize: SearchSelectors.targetImageSize(searchState),
        targetImageUploadingError: SearchSelectors.targetImageUploadingError(searchState),
        searchResultList: SearchSelectors.searchResultList(searchState),
        onImageUploaded: (e) => searchSimilarImagesByFile(e.target.files[0]),
        onSearchByURLActivated: (e) => searchSimilarImagesByURL(e.target.value)
    };
})(Search);
