import './search_result_list.scss';
import {Component} from 'react';

export default class SearchResultList extends Component {
    render() {
        const list = this.props.list;
        let result = null;

        if (list.size) {
            result = (
                <div className="search-result-list-container">
                    <div className="search-result-list-caption"> Images visually similar to the sought-for one: </div>
                    <div className="search-result-list">
                        {
                            list
                                .map((imgURL) => {
                                    return <div><img src={imgURL} className="tile" /></div>;
                                })
                        }
                    </div>
                </div>
            );
        }

        return result;
    }
}
