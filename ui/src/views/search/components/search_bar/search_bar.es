import './search_bar.scss';
import { Component } from 'react';

export default class search_bar extends Component {
    render() {
        return (
            <div className="search-bar-container">
                <input ref="dialog" type="file" onChange={ this.props.onImageUploaded } />
                <div className="search-bar">
                    <input type="text" placeholder="Upload an image or just enter one's URL..." />
                    <span className="image-uploader" onClick={ () => this.refs.dialog.click() }></span>
                    <div className="search-button"></div>
                </div>
            </div>
        );
    }
}
