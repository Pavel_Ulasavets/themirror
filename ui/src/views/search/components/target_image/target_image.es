import './target_image.scss';
import {Component} from 'react';

export default class TargetImage extends Component {
    render() {
        const { src, size, error } = this.props;
        let imgComponent;
        let imgDescription;
        let errorMessage;

        if (src) {
            imgComponent = <img src={src} className="target-image"/>;
            imgDescription = (
                <div className="target-image-description">
                    <div>Image size: <span> {size}</span></div>
                </div>
            );
        } else if (error) {
            errorMessage = <div className="err-message">{ error }</div>;
        }

        return (
            <div className="target-image-container">
                {imgComponent}
                {imgDescription}
                {errorMessage}
            </div>
        );
    }
}
