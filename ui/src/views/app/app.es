import './app.scss';
import { Component } from 'react';
// child components
import Header from './components/header/header';
import AppBody from './components/app_body/app_body';

export default class App extends Component {
    render() {
        return (
            <div>
                <Header />
                <AppBody>{ this.props.children }</AppBody>
            </div>
        );
    }
}
