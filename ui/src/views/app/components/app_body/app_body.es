import './app_body.scss';
import { Component } from 'react';

export default class AppBody extends Component {
    render() {
        return (
            <div className="app-body">
                { this.props.children }
            </div>
        );
    }
}
