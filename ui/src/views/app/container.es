import { Component } from 'react';

export default class AppContainer extends Component {
    // Probably, some layout stuff should be implemented namely here
    render() {
        return (
            <div className="app-container">
                { this.props.children }
            </div>
        );
    }
}
