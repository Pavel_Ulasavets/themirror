// styles
import 'shared/styles/form.scss';
import { Component } from 'react';
// components
import { SocialButtonPanel, ButtonTypes } from 'shared/components/social-button-panel';
// actions
import { authenticateUser } from '../../actions/user_actions';
// constants
import { SocialsSettings } from '../../../configuration';

export default class Login extends Component {
    tryToAuthenticate() {
        const credentials = {
            email: this.refs.email.value,
            password: this.refs.password.value
        };

        // maybe the form validation should be here
        authenticateUser(credentials);
    }

    render() {
        const { user = {}, errors = {} } = this.props;
        const fields = [
            { name: 'email', placeholder: 'Email', type: 'text' },
            { name: 'password', placeholder: 'Password', type: 'password' }
        ];
        const buttons = [
            { type: ButtonTypes.google, text: SocialsSettings.google.text, url: SocialsSettings.google.url },
            { type: ButtonTypes.instagram, text: SocialsSettings.instagram.text, url: SocialsSettings.instagram.url }
        ];

        return (
            <form className='form'>
                <h2>Login</h2>
                <SocialButtonPanel buttons={buttons}/>
                {
                    fields.map((field) => (
                        <div key={`${field.name}-group`}>
                            <input ref={field.name} value={user[field.name]} placeholder={field.placeholder} type={field.type} />
                            {
                                (() => {
                                    if (errors[field.name]) {
                                        <label className='err-label' for={field.name}>{errors[field.name].msg}</label>;
                                    }
                                })()
                            }
                        </div>
                    ))
                }
                <input type='button' value='Log in' onClick={ () => this.tryToAuthenticate() } />
            </form>
        );
    }
}
