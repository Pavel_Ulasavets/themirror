import store from '../store/app_store';
// constants
import { maxSizeOfUploadedImageInBytes, supportedImageExtensions } from '../constants/search_constants';
// action types
import ActionTypes from '../action_types/search_action_types';
// services
import { searchSimilarImages } from '../services/search_service';
// helpers
import { normalizeFileSize } from 'shared/helpers/system_helpers';

/**
 * sets a sought-for image
 * @param {String} targetImage
 */
function setTargetImage(targetImage) {
    store.dispatch({
        type: ActionTypes.SET_TARGET_IMAGE,
        payload: { targetImage }
    });
}

/**
 * performs the search of the specified image among other images in the application
 */
function setSearchResultList(searchResultList) {
    store.dispatch({
        type: ActionTypes.SET_SEARCH_RESULT_LIST,
        payload: { searchResultList }
    });
}

/**
 * seeks all images that are similar to the specified one
 * @param  {File} imgFile - a sought-for image
 */
export function searchSimilarImagesByFile(imgFile) {
    const hasAcceptableSize = imgFile.size < maxSizeOfUploadedImageInBytes;
    const hasAcceptableExtension = new RegExp(`\.(${supportedImageExtensions.join('|')})$`).test(imgFile.name);

    if (!hasAcceptableSize) {
        return setTargetImage({ error: 'The image is too big!' });
    } else if (!hasAcceptableExtension) {
        return setTargetImage({ error: `${imgFile.name} has an unacceptable extension. `});
    }

    const reader = new FileReader();

    reader.onload = (e) => {
        const imgDataURL = e.target.result;

        setTargetImage({ url: imgDataURL, size: normalizeFileSize(imgFile.size) });
        searchSimilarImages(imgDataURL)
            .then((images) => {
                setSearchResultList(images);
            });
    };

    reader.onerror = (e) => {
        // TODO: log error
    };

    reader.readAsDataURL(imgFile);
}

/**
 * seeks all similar images by specified image url
 * @param  {String} url
 */
export function searchSimilarImagesByURL(url) {

}
