import * as UserService from '../services/user_service';
// actions
import { goToFeedPage } from './router_actions';
/**
 * register a specified user on the server
 * @param  {Object} user - a user to be registered
 */
export function registerUser(user) {
    UserService
        .registerUser(user)
        .then(
            function onUserRegistered() {
                goToFeedPage();
            },
            function onUserRegistrationFailed() {
                /*eslint-disable*/
                console.log(`Registration of ${user} has failed`);
                /*eslint-enable*/
            }
        );
}

/**
 * authenticate a specified user
 * @param  {Object} credentials - credentials to authenticate with
 */
export function authenticateUser(credentials) {
    UserService
        .authenticateUser(credentials)
        .then(
            function onUserAuthenticated() {
                goToFeedPage();
            },
            function onUserAuthenticationFailed() {
                /*eslint-disable*/
                console.log(`Authentication of ${credentials} has failed`);
                /*eslint-enable*/
            }
        );
}
