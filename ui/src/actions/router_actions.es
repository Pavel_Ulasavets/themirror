import { getHistory } from '../routes/history';

const history = getHistory();

export function goToFeedPage() {
    history.push('/feed');
}

export function goToLoginPage() {
    history.push('/login');
}

export function goToRegisterPage() {
    history.push('/register');
}

window.goToFeedPage = goToFeedPage;
