import { Route } from 'react-router';
// components
import AppContainer from '../views/app/container';
import App from '../views/app/app';
import Login from '../views/login/login';
import Register from '../views/register/register';
import Feed from '../views/feed/feed';
import Search from '../views/search/search';

export function getRoutes() {
    return (
        <Route path="/" component={AppContainer}>
            <Route path='/login' component={Login} />
            <Route path='/register' component={Register} />
            <Route component={App}>
                <Route path='/feed' component={Feed} />
                <Route path='/search' component={Search} />
            </Route>
        </Route>
    );
}
