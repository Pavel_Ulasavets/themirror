import { browserHistory } from 'react-router';

// this is a kind of abstraction around the react browser history singleton
// this layer is introduced only to have no explicit dependence to the concrete module
export function getHistory() {
    return browserHistory;
}
