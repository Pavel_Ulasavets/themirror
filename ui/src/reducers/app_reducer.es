import Immutable from 'immutable';
import _ from 'lodash';
// reducers
import searchReducer from './search_reducer';

const reducers = {
    search: searchReducer
};

export default function appReducer(state = Immutable.Map(), action) {
    return _.reduce(
        reducers,
        (currentState, reducer, key) => {
            return currentState.update(key, (statePart) => reducer(statePart, action));
        },
        state
    );
}
