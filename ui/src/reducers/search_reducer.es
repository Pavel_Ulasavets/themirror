import Immutable from 'immutable';
// action types
import ActionTypes from '../action_types/search_action_types';

const defaultState = Immutable.Map({
    targetImage: Immutable.Map({
        url: null,
        size: null,
        error: null,
    }),
    searchResultList: Immutable.List()
});

export default function (state = defaultState, { type, payload }) {
    switch (type) {
        case ActionTypes.SET_TARGET_IMAGE:
            return state.set('targetImage', Immutable.fromJS(payload.targetImage));
        case ActionTypes.SET_SEARCH_RESULT_LIST:
            return state.set('searchResultList', Immutable.List(payload.searchResultList));
        default:
            return state;
    }
};
