// external dependencies
import _ from 'lodash';
import promisify from 'es6-promisify';
import { genSaltSync, hashSync } from 'bcryptjs';
import mongoose from 'mongoose';
// internal files/modules
import {Schema} from 'mongoose';
import {PictureSchema} from './picture';
// helpers
import { pHash, hammingDistance } from '../helpers/image_helpers';
// constants
import { SearchConstants } from '../constants';

const getPixels = promisify(require('get-pixels'));

export const UserSchema = new mongoose.Schema({
    id: Schema.Types.String,
    email: Schema.Types.String,
    fullname: Schema.Types.String,
    username: Schema.Types.String,
    salt: Schema.Types.String,
    password: Schema.Types.String,
    subscriptions: [Schema.Types.ObjectId],
    token: Schema.Types.String,
    pictures: [PictureSchema],
    logo: PictureSchema
});

const UserModelProxy = { UserModel: null };

UserSchema.statics.getAllUsers = function getAllUsers() {
    return UserModelProxy.UserModel.find({})
        .then(
            (users) => {
                return users;
            },
            (err) => {
                /*eslint-disable*/console.error(err);/*eslint-enable*/
            }
        );
};

UserSchema.statics.findUserById = function findUserById(id) {
    return UserModelProxy.UserModel.findOne({ id });
};

UserSchema.statics.serializeUser = function serialize(user) {
    return Promise.resolve(user._id);
};

UserSchema.statics.deserializeUser = function deserialize(_id) {
    return UserModelProxy.UserModel.findOne({ _id})
        .then(
            (user) => { return user; },
            (err) => {
               /*eslint-disable*/console.error(err);/*eslint-enable*/
            }
        );
};

UserSchema.statics.verifyCredentials = function verifyCredentials({email, password}) {
    return UserModelProxy.UserModel
        .findOne({ email })
        .then((user) => {
            if (!user) {
                throw new Error(`There is no user registered with such email ${email}`);
            } else {
                const salt = user.salt;
                const hashedPassword = hashSync(password, salt);
                const userPassword = user.password;

                if (hashedPassword === userPassword) {
                    delete user.salt;
                    delete user.password;
                    return user;
                } else {
                    throw new Error('Password is incorrect');
                }
            }
        });
};

UserSchema.statics.registerUserDirectly = function ({fullname, email, password}) {
    return UserModelProxy.UserModel
        .findOne({ email })
        .then((user) => {
            if (user) {
                throw new Error(`The user with such email already exists`);
            } else {
                const salt = genSaltSync();
                const hashedPassword = hashSync(password, salt);
                return UserModelProxy.UserModel.create({fullname, email, salt, password: hashedPassword});
            }
        });
};

UserSchema.statics.registerUserIndirectly = function (user) {
    return UserModelProxy.UserModel.create(user);
};

UserSchema.statics.updateUser = function updateUser(id, userData) {
    return UserModelProxy.UserModel
        .update({id}, userData)
        .then(
            (data) => {
                return data;
            },
            (err) => {
                /*eslint-disable*/console.error(err);/*eslint-enable*/
            }
        );
};

UserSchema.statics.removeUser = function removeUser(id) {
    return UserModelProxy.UserModel('User').remove({id});
};

UserSchema.statics.getAllImages = function getAllImages() {
    return UserModelProxy.UserModel
        .find()
        .$where(function () { return this.pictures.length > 0; })
        .select('pictures.imagePath')
        .then(
            (users) => {
                return users.reduce(
                    (images, user) => {
                        const userImages = user.pictures.map((img) => img.imagePath);

                        return images.concat(userImages);
                    },
                    []
                );
            }
        );
}

UserSchema.statics.findSimilarImages = function findSimilarImages(dataURI) {
    return UserModelProxy.UserModel.getAllImages()
        .then((images) => {
            let imgPromises = [];

            for (let img of images) {
                imgPromises.push(
                    getPixels(img).then((pixels) => pHash(pixels)).then((hash) => ({ imgPath: img, pHash: hash }))
                );
            }

            return Promise.all(imgPromises);
        })
        .then((images) => {
            const targetHashPromise = getPixels(dataURI).then((pixels) => pHash(pixels));
            return Promise.all([targetHashPromise, Promise.resolve(images)]);
        })
        .then(([targetHash, images]) => {
            return _(images)
                .filter((img) => hammingDistance(targetHash, img.pHash) < SearchConstants.deviationLimit)
                .map((img) => img.imgPath)
                .value();
        });
};

export const UserModel = UserModelProxy.UserModel = mongoose.model('User', UserSchema);


