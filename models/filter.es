import mongoose from 'mongoose';
import {Schema} from 'mongoose';

export const FilterSchema = new mongoose.Schema({
    name: Schema.Types.String
});

const FilterModelProxy = { FilterModel: null };

FilterSchema.statics.getFilters = function getFilters() {
    return FilterModelProxy.FilterModel
        .find({})
        .then(
            (data) => {
                return data;
            },
            (err) => {
                /*eslint-disable*/console.error(err);/*eslint-enable*/
            }
        );
}

export const FilterModel = FilterModelProxy.FilterModel = mongoose.model('Filter', FilterSchema);
