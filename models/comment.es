import mongoose from 'mongoose';
import {Schema} from 'mongoose';

export const CommentSchema = new mongoose.Schema({
    text: Schema.Types.String,
    authorId: [Schema.Types.ObjectId]
});

CommentSchema.add({
    comments: [CommentSchema]
});

export const CommentModel = mongoose.model('Comment', CommentSchema);

