import mongoose from 'mongoose';
import {Schema} from 'mongoose';

import {CommentSchema} from './comment';

export const PictureSchema = new mongoose.Schema({
    name: Schema.Types.String,
    likedBy: [Schema.Types.ObjectId],
    comments: [CommentSchema],
    appliedFilters: [Schema.Types.ObjectId],
    imagePath: Schema.Types.String
});

export const PictureModel = mongoose.model('Picture', PictureSchema);
